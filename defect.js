const express = require('express');
var bodyparser = require('body-parser');
var morgan = require('morgan');
var helpers = require('./helpers').helpers;
var port = process.env.PORT || 5000;

var app = express()

//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

app.use(morgan('dev'));


require('./router').establishRoutes(app, helpers);

app.listen(port)
console.log('port number 5000 is running')