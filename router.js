    (function() {

    function establishRoutes(app, helpers) {

        
        app.post('/create', function(rqst, res, next) {
            require('./api/defect_create').execute(rqst, res, next, helpers);
        });

        app.get('/list', function(req, res, next) {
            require('./api/defect_create').getdata(req, res, next, helpers);
        });        
        
    }

    exports.establishRoutes = establishRoutes;
})()
