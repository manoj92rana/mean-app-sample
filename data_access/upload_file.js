(function() {

        function getDefectList(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Defect = dbClient.collection('defect_upload');
                Defect.find(query).toArray(function(retrievalErr, defect_upload) {      
                console.log(retrievalErr)              
                    if (!retrievalErr) {
                        helpers.execute(cb, [defect_upload]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });
            } else { 
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }
    function createDefect(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Defect = dbClient.collection('defect_upload');
                Defect.insertOne(query,function(retrievalErr, defect_upload) {                    
                    if (!retrievalErr) {
                        helpers.execute(cb, [query, defect_upload]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }
     
     function updateDefect(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Managers = dbClient.collection('managers');
                Managers.findOneAndUpdate(query,options,function(retrievalErr, managers) {                    
                    if (!retrievalErr) {
                        helpers.execute(cb, [query, managers]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }


    exports.Defect = {
        getDefectList: getDefectList, 
        createDefect:createDefect
    }

})();